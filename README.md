# Un-Americanise

## Description

This is a tool that takes in English text spelt using American English and converts it to British English.

## Miscellaneous

See [this announcement](https://www.reddit.com/r/haskell/comments/9l5gyi/fyi_cabal24_supports_sourcerepositorypackage_for/) for usage of off-Hackage dependencies in `build-depends`.

## Error received when building with chatter

Observed on 2024-02-07 ([link to Cabal file](https://gitlab.com/ikoh/unamericanise/-/blob/main/unamericanise.cabal)):

```console
Failed to build chatter-0.1.0.5.
Build log ( /Users/ikoh/.cabal/logs/ghc-9.4.8/chttr-0.1.0.5-59097a21.log ):
Configuring library for chatter-0.1.0.5..
Preprocessing library for chatter-0.1.0.5..
Building library for chatter-0.1.0.5..
[ 1 of 11] Compiling Data.DefaultMap  ( src/Data/DefaultMap.hs, dist/build/Data/DefaultMap.o, dist/build/Data/DefaultMap.dyn_o )
[ 2 of 11] Compiling NLP.Corpora.Email ( src/NLP/Corpora/Email.hs, dist/build/NLP/Corpora/Email.o, dist/build/NLP/Corpora/Email.dyn_o )

src/NLP/Corpora/Email.hs:27:16: error:
    • Couldn't match type ‘Data.Text.Internal.Lazy.Text’ with ‘Text’
      Expected: Message -> Text
        Actual: Message -> Data.Text.Internal.Lazy.Text
      NB: ‘Text’ is defined in ‘Data.Text.Internal’
          ‘Data.Text.Internal.Lazy.Text’
            is defined in ‘Data.Text.Internal.Lazy’
    • In the first argument of ‘map’, namely ‘body’
      In the second argument of ‘($)’, namely ‘map body archive’
      In a stmt of a 'do' block: return $ map body archive
   |
27 |   return $ map body archive
   |                ^^^^

src/NLP/Corpora/Email.hs:32:28: error:
    • Couldn't match type ‘Data.Text.Internal.Lazy.Text’ with ‘Text’
      Expected: Message -> Text
        Actual: Message -> Data.Text.Internal.Lazy.Text
      NB: ‘Text’ is defined in ‘Data.Text.Internal’
          ‘Data.Text.Internal.Lazy.Text’
            is defined in ‘Data.Text.Internal.Lazy’
    • In the second argument of ‘(.)’, namely ‘body’
      In the first argument of ‘map’, namely ‘(tokenize . body)’
      In the second argument of ‘($)’, namely
        ‘map (tokenize . body) archive’
   |
32 |   return $ map (tokenize . body) archive
   |                            ^^^^

src/NLP/Corpora/Email.hs:39:32: error:
    • Couldn't match type ‘Text’ with ‘Data.Text.Internal.Lazy.Text’
      Expected: [Data.Text.Internal.Lazy.Text]
        Actual: [Text]
      NB: ‘Data.Text.Internal.Lazy.Text’
            is defined in ‘Data.Text.Internal.Lazy’
          ‘Text’ is defined in ‘Data.Text.Internal’
    • In the second argument of ‘concatMap’, namely ‘contents’
      In the second argument of ‘($)’, namely
        ‘concatMap parseMBox contents’
      In a stmt of a 'do' block: return $ concatMap parseMBox contents
   |
39 |   return $ concatMap parseMBox contents
   |                                ^^^^^^^^
[ 3 of 11] Compiling NLP.Types        ( src/NLP/Types.hs, dist/build/NLP/Types.o, dist/build/NLP/Types.dyn_o )
[ 4 of 11] Compiling NLP.Similarity.VectorSim ( src/NLP/Similarity/VectorSim.hs, dist/build/NLP/Similarity/VectorSim.o, dist/build/NLP/Similarity/VectorSim.dyn_o )
[ 5 of 11] Compiling NLP.POS.LiteralTagger ( src/NLP/POS/LiteralTagger.hs, dist/build/NLP/POS/LiteralTagger.o, dist/build/NLP/POS/LiteralTagger.dyn_o )
[ 6 of 11] Compiling NLP.POS.UnambiguousTagger ( src/NLP/POS/UnambiguousTagger.hs, dist/build/NLP/POS/UnambiguousTagger.o, dist/build/NLP/POS/UnambiguousTagger.dyn_o )
[ 7 of 11] Compiling NLP.POS.AvgPerceptron ( src/NLP/POS/AvgPerceptron.hs, dist/build/NLP/POS/AvgPerceptron.o, dist/build/NLP/POS/AvgPerceptron.dyn_o )

src/NLP/POS/AvgPerceptron.hs:86:10: error:
    • No instance for (NFData Feature)
        arising from a use of ‘Control.DeepSeq.$dmrnf’
    • In the expression: Control.DeepSeq.$dmrnf @(Perceptron)
      In an equation for ‘Control.DeepSeq.rnf’:
          Control.DeepSeq.rnf = Control.DeepSeq.$dmrnf @(Perceptron)
      In the instance declaration for ‘NFData Perceptron’
   |
86 | instance NFData Perceptron
   |          ^^^^^^^^^^^^^^^^^
[ 8 of 11] Compiling NLP.Corpora.Parsing ( src/NLP/Corpora/Parsing.hs, dist/build/NLP/Corpora/Parsing.o, dist/build/NLP/Corpora/Parsing.dyn_o )
[10 of 11] Compiling Paths_chatter    ( dist/build/autogen/Paths_chatter.hs, dist/build/Paths_chatter.o, dist/build/Paths_chatter.dyn_o )
Error: cabal: Failed to build chatter-0.1.0.5 (which is required by
exe:unamericanise from unamericanise-0.1.0.0). See the build log above for
details.
```